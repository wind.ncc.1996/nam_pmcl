## HOW TO INSTALL PROJECT
### Clone project from gitlab
### Install Dependencies
 - Python 3.8
 - pip 1.19.x
 - Setup virtual env: https://docs.python.org/3/library/venv.html
 - Run `pip install -r requirements.txt`
 
### Migrate database
 - Run `python manage.py migrate`
### Run server
 - Run `python manage.py runserver`
 
### How to use
 - Create watermark
   ```javascript
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/x-www-form-urlencoded");
    
    var formdata = new FormData();
    formdata.append("input_image", fileInput.files[0], "/C:/Users/Cuong Nguyen/Desktop/Capture.PNG");
    formdata.append("pdf_file", fileInput.files[0], "/C:/Users/Cuong Nguyen/Desktop/New Text Document.txt");
    formdata.append("x0", "1");
    formdata.append("x1", "2");
    formdata.append("y0", "3");
    formdata.append("y1", "4");
    
    var requestOptions = {
      method: 'POST',
      headers: myHeaders,
      body: formdata,
      redirect: 'follow'
    };
    
    fetch("http://localhost:8000/upload/image-watermark/", requestOptions)
      .then(response => response.text())
      .then(result => console.log(result))
      .catch(error => console.log('error', error));
    ```
 
 - Watermark history
    ```javascript
        var requestOptions = {
          method: 'GET',
          redirect: 'follow'
        };
        
        fetch("http://localhost:8000/upload/image-watermark-history/", requestOptions)
          .then(response => response.text())
          .then(result => console.log(result))
          .catch(error => console.log('error', error));
    ```
