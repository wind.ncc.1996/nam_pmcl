from django.urls import path
from .views import PDFWatermarkUploadView, ImageWatermarkUploadView, ImageWatermarkHistory

urlpatterns = [
    path('pdf-watermark/', PDFWatermarkUploadView.as_view()),
    path('image-watermark/', ImageWatermarkUploadView.as_view()),
    path('image-watermark-history/', ImageWatermarkHistory.as_view()),
]
