from django.db import models


class PDFWatermark(models.Model):
    input_pdf_file = models.FileField()
    input_logo = models.ImageField()
    output_pdf_file = models.FileField(null=True, blank=True)

    class Meta:
        db_table = 'pdf_watermark'


class ImageWatermark(models.Model):
    input_image = models.ImageField()
    pdf_file = models.FileField()
    output_image = models.ImageField(null=True, blank=True)

    class Meta:
        db_table = 'image_watermark'


