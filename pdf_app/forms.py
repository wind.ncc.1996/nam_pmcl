from .models import PDFWatermark, ImageWatermark
from .helper import extract_watermark
from django import forms


class PDFWatermarkForm(forms.ModelForm):

    class Meta:
        model = PDFWatermark
        fields = ('input_pdf_file', 'input_logo')


class ImageWatermarkForm(forms.ModelForm):

    x0 = forms.FloatField()
    x1 = forms.FloatField()
    y0 = forms.FloatField()
    y1 = forms.FloatField()

    class Meta:
        model = ImageWatermark
        fields = ('input_image', 'pdf_file', 'x0', 'x1', 'y0', 'y1')

    def extract_watermark(self):
        instance = self.instance
        clean_data = self.cleaned_data
        output = extract_watermark(
            instance.input_image, instance.pdf_file,
            clean_data['x0'], clean_data['x1'],
            clean_data['y0'], clean_data['y1']
        )
        self.instance.output_image.save(*output)
        return self.save()


