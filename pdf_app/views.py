from django.http import JsonResponse
from django.views import View

from .forms import PDFWatermarkForm, ImageWatermarkForm
from.models import ImageWatermark


class PDFWatermarkUploadView(View):

    def post(self, request):
        form = PDFWatermarkForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            data = {
                'success': True
            }
        else:
            data = {
                'error': form.errors
            }
        return JsonResponse(data)


class ImageWatermarkUploadView(View):

    def post(self, request):
        form = ImageWatermarkForm(request.POST, request.FILES)
        if form.is_valid():
            instance = form.extract_watermark()
            data = {
                'url': request.build_absolute_uri(instance.output_image.url)
            }
        else:
            data = {
                'error': form.errors
            }

        return JsonResponse(data)


class ImageWatermarkHistory(View):

    def get(self, request):
        data = []
        qs = ImageWatermark.objects.all()
        for o in qs:
            if o.output_image:
                data.append({
                    "id": o.id,
                    "url": request.build_absolute_uri(o.output_image.url)
                })

        return JsonResponse(data, safe=False)
